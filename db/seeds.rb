start = 1.day.ago
Weight.create(can: 1, weight: 0, created_at: start)
previous = 0
for min in 1..59
  current = Random.rand(500) + previous
  Weight.create(can: 1, weight: current, created_at: start + min.minutes)
  previous = current
end

later = start + 1.hour
Weight.create(can: 1 , weight: 0, created_at: later)
previous = 0
for min in 1..59
  current = Random.rand(500) + previous
  Weight.create(can: 1, weight: current, created_at: later + min.minutes)
  previous = current
end
