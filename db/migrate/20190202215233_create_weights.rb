class CreateWeights < ActiveRecord::Migration[5.2]
  def change
    create_table :weights do |t|
      t.integer :can
      t.integer :weight

      t.timestamps
    end
  end
end
