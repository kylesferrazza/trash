#!/usr/bin/env python2

from smbus import SMBus
from sys import exit
import requests
import time

epsilon = 20
can = 1
server_address = "trashapp.org"

#I2C ADDRESS/BITS

MPL3115A2_ADDRESS = (0x60)

#REGISTERS

MPL3115A2_REGISTER_STATUS = (0x00)
MPL3115A2_REGISTER_STATUS_TDR = 0x02
MPL3115A2_REGISTER_STATUS_PDR = 0x04
MPL3115A2_REGISTER_STATUS_PTDR = 0x08

MPL3115A2_REGISTER_PRESSURE_MSB = (0x01)
MPL3115A2_REGISTER_PRESSURE_CSB = (0x02)
MPL3115A2_REGISTER_PRESSURE_LSB = (0x03)

MPL3115A2_REGISTER_TEMP_MSB = (0x04)
MPL3115A2_REGISTER_TEMP_LSB = (0x05)

MPL3115A2_REGISTER_DR_STATUS = (0x06)

MPL3115A2_OUT_P_DELTA_MSB = (0x07)
MPL3115A2_OUT_P_DELTA_CSB = (0x08)
MPL3115A2_OUT_P_DELTA_LSB = (0x09)

MPL3115A2_OUT_T_DELTA_MSB = (0x0A)
MPL3115A2_OUT_T_DELTA_LSB = (0x0B)

MPL3115A2_BAR_IN_MSB = (0x14)

MPL3115A2_WHOAMI = (0x0C)

#BITS

MPL3115A2_PT_DATA_CFG = 0x13
MPL3115A2_PT_DATA_CFG_TDEFE = 0x01
MPL3115A2_PT_DATA_CFG_PDEFE = 0x02
MPL3115A2_PT_DATA_CFG_DREM = 0x04

MPL3115A2_CTRL_REG1 = (0x26)
MPL3115A2_CTRL_REG1_SBYB = 0x01
MPL3115A2_CTRL_REG1_OST = 0x02
MPL3115A2_CTRL_REG1_RST = 0x04
MPL3115A2_CTRL_REG1_OS1 = 0x00
MPL3115A2_CTRL_REG1_OS2 = 0x08
MPL3115A2_CTRL_REG1_OS4 = 0x10
MPL3115A2_CTRL_REG1_OS8 = 0x18
MPL3115A2_CTRL_REG1_OS16 = 0x20
MPL3115A2_CTRL_REG1_OS32 = 0x28
MPL3115A2_CTRL_REG1_OS64 = 0x30
MPL3115A2_CTRL_REG1_OS128 = 0x38
MPL3115A2_CTRL_REG1_RAW = 0x40
MPL3115A2_CTRL_REG1_ALT = 0x80
MPL3115A2_CTRL_REG1_BAR = 0x00
MPL3115A2_CTRL_REG2 = (0x27)
MPL3115A2_CTRL_REG3 = (0x28)
MPL3115A2_CTRL_REG4 = (0x29)
MPL3115A2_CTRL_REG5 = (0x2A)

MPL3115A2_REGISTER_STARTCONVERSION = (0x12)


bus = SMBus(1)

whoami = bus.read_byte_data(MPL3115A2_ADDRESS, MPL3115A2_WHOAMI)
if whoami != 0xc4:
    print "Device not active."
    exit(1)

bus.write_byte_data(
    MPL3115A2_ADDRESS,
    MPL3115A2_CTRL_REG1,
    MPL3115A2_CTRL_REG1_SBYB |
    MPL3115A2_CTRL_REG1_OS128 |
    MPL3115A2_CTRL_REG1_ALT)

bus.write_byte_data(
    MPL3115A2_ADDRESS,
    MPL3115A2_PT_DATA_CFG,
    MPL3115A2_PT_DATA_CFG_TDEFE |
    MPL3115A2_PT_DATA_CFG_PDEFE |
    MPL3115A2_PT_DATA_CFG_DREM)

def poll():
    sta = 0
    while not (sta & MPL3115A2_REGISTER_STATUS_PDR):
        sta = bus.read_byte_data(MPL3115A2_ADDRESS, MPL3115A2_REGISTER_STATUS)


def pressure():
    bus.write_byte_data(
        MPL3115A2_ADDRESS,
        MPL3115A2_CTRL_REG1,
        MPL3115A2_CTRL_REG1_SBYB |
            MPL3115A2_CTRL_REG1_OS128 |
            MPL3115A2_CTRL_REG1_BAR)

    poll()

    msb, csb, lsb = bus.read_i2c_block_data(MPL3115A2_ADDRESS,MPL3115A2_REGISTER_PRESSURE_MSB,3)
    #print msb, csb, lsb

    return ((msb<<16) | (csb<<8) | lsb) / 64.

def calibrate():
    pa = int(pressure()/2)
    bus.write_i2c_block_data(MPL3115A2_ADDRESS, MPL3115A2_BAR_IN_MSB, [pa>>8 & 0xff, pa & 0xff])

def send(p):
    print("sending current pressure")
    d = {"can": can, "pressure": p}
    r = requests.post('http://the-trash-app.herokuapp.com/api', data = d)

if __name__ == "__main__":
    calibrate()
    pressure()
    cur = pressure()
    while True:
        time.sleep(1)
        newp = pressure()
        if abs(newp - cur) >= epsilon:
            send(newp)
        cur = newp
