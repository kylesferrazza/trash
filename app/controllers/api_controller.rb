class ApiController < ActionController::Base
  protect_from_forgery with: :null_session
  skip_before_action :verify_authenticity_token

  def ptow(pressure)
    pressure_change = pressure - 102395
    return [((pressure_change * 67) / 9.81), 0].max
  end

  def update
    if not params[:can] or not params[:pressure]
      render json: "bad request. please supply can and pressure"
      return
    end
    can = params.require(:can)
    pressure = params.require(:pressure)
    weight = ptow(pressure.to_i)
    record = Weight.new(can: can, weight: weight)
    if record.save
      render json: "created.\n"
    else
      render json: "error saving weight."
    end
  end

  def show
    all = Weight.where(can: 1)
    result = []
    @labels = []
    all.each do |weight|
      @labels.append(weight.created_at.to_s)
      result.append({"x" => weight.created_at.to_i, "y" => weight.weight})
    end
    @labels = @labels.to_json.html_safe
    @records = result.to_json.html_safe
  end
end
