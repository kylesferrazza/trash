# Trash App

Track your daily trash generation to become a more environmentally-friendly trash producer.

The app will show you graphs depicting your trash generation and scores based on your trash to recycling ratio.

Accompanied with this rails app is a raspberry pi with an equipped pressure sensor.

The pressure sensor sits in a bag full of air at the bottom of the trash can.

Pressure readings are sent from the raspberry pi to this rails server, which compiles the data and creates the charts.

This project was made for MakeHarvard 2019 by Kyle Sferrazza, Adam Potter, Shahir Rahman, and Jacob Chvatal.

![demo graph](vendor/img/graph.png)

![can](vendor/img/can.jpg)

![pi](vendor/img/pi.jpg)

# UI Mockup

![pi](vendor/img/mockup.png)
